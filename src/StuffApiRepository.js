import RestApiRepository from "restful/dist/src/RestApiRepository";
import Axios from "axios";


export default class StuffApiRepository extends RestApiRepository {
    constructor(basePath, urlMap) {
        super(basePath + urlMap['base']);
        this.bPath = basePath;
        this.urlMap = urlMap;
    };

    search(query, params) {
        var path = this.bPath + this.urlMap['search'];
        params['name'] = query;

        if(params) {
            path += this.queryFromDict(params);
        }

        path = encodeURI(path);

        return new Promise(
            (resolve, reject) => {
                Axios.get(path).then(
                    (data) => {
                        if(!data['data']) {
                            reject();
                        } else {
                            resolve(data.data);
                        }
                    }
                ).catch(
                    (data) => {
                        reject(data);
                    }
                )
            }
        );
    };

    pickup(id, params) {
        var path = this.basePath + '/pickup/' + id;

        if(params) {
            path += this.queryFromDict(params);
        }

        path = encodeURI(path);

        return new Promise(
            (resolve, reject) => {
                Axios.post(path).then(
                    (data) => {
                        if(!data['data']) {
                            reject();
                        } else {
                            resolve(data.data);
                        }
                    }
                ).catch(
                    (data) => {
                        reject(data);
                    }
                )
            }
        );
    }

    put(id, params) {
        var path = this.basePath + '/put/' + id;

        if(params) {
            path += this.queryFromDict(params);
        }

        path = encodeURI(path);

        return new Promise(
            (resolve, reject) => {
                Axios.post(path).then(
                    (data) => {
                        if(!data['data']) {
                            reject();
                        } else {
                            resolve(data.data);
                        }
                    }
                ).catch(
                    (data) => {
                        reject(data);
                    }
                )
            }
        );
    }
}