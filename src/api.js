import Axios from "axios";

export default class Api {
    constructor(apiServer, apiPrefix, clientId, clientSecret) {
        this.apiServer = apiServer;
        this.apiPrefix = apiPrefix;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
    }

    getStuffList(expand) {
        let params = {
            'per-page': 1000,
            'sort' : '-id'
        };

        if(expand) {
            params['expand'] = expand.join(',');
        }
        return this.request('get', 'stuff', false, params);
    }

    getStuff(id, expand) {
        let params = {};
        if(expand) {
            params['expand'] = expand.join(',');
        }

        return this.request('get', 'stuff/' + id, false, params);
    }

    createStuff(data) {
        let formData = new FormData();
        for(var i in data) {
            if(typeof data[i] === 'object') {
                for(var a in data[i]) {
                    formData.append(i + '[]', data[i][a]);
                }
            } else {
                formData.append(i, data[i]);
            }
        }

        return this.request('post', 'stuff', false, formData);
    }

    updateStuff(data) {
        let formData = new FormData();
        for(var i in data) {
            if(typeof data[i] === 'object') {
                for(var a in data[i]) {
                    formData.append(i + '[]', data[i][a]);
                }
            } else {
                formData.append(i, data[i]);
            }
        }

        return this.request('put', 'stuff', false, formData);
    }

    deleteStuff(id) {
        return this.request('delete', 'stuff', false);
    }

    createImage(data) {
        let formData = new FormData();
        formData.append('image', data['image']);

        return this.request('post', 'image', false, formData);
    }

    getContainersList(expand) {
        let params = {
            'per-page': 1000
        };

        if(expand) {
            params['expand'] = expand.join(',');
        }
        return this.request('get', 'container', false, params);
    }

    getSectionsList(expand) {
        let params = {
            'per-page': 1000
        };

        if(expand) {
            params['expand'] = expand.join(',');
        }
        return this.request('get', 'section', false, params);
    }

    request(type, path, apiKeyAuth, data) {
        var url = this.apiServer + this.apiPrefix + path;

        if(type == 'get') {
            return Axios.get(
                url,
                {
                    params: data
                }
            )
        } else if(type == 'post') {
            return Axios.post(
                url,
                data,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            )
        } else if(type == 'delete') {
            return Axios.delete(
                url
            )
        } else if(type == 'put') {
            return Axios.put(
                url,
                data,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                }
            )
        }

    }
}