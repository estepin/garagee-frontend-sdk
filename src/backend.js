import Api from "./api";
import RestApi from "restful";
import RestApiRepository from "restful/dist/src/RestApiRepository";
import StuffApiRepository from "./StuffApiRepository";


export default class Backend {

    constructor(apiServer, apiPrefix, clientId, clientSecret) {
        this.apiServer = apiServer;
        this.api = new Api(apiServer, apiPrefix, clientId, clientSecret);

        this.restApi = new RestApi({
            repositories: {
                stuff: new StuffApiRepository(apiServer + apiPrefix, {base: 'stuff', search: 'search-stuff'}),
                container: new RestApiRepository(apiServer + apiPrefix + 'container'),
                section: new RestApiRepository(apiServer + apiPrefix + 'section'),
                image: new RestApiRepository(apiServer + apiPrefix + 'image'),
            }
        });
    }

    getStuffList(expand, filter, perPage, page) {
        var params = {expand, sort: "-updated_at"};
        if(perPage) {
            params['per-page'] = perPage;
        }

        if(filter) {
            if(filter['container']) {
                params['filter_container'] = filter['container'];
            }

            if(filter['section']) {
                params['filter_section'] = filter['section'];
            }
        }

        if(page) {
            params['page'] = page;
        }

        return this.restApi.repository('stuff').getAll(params);
    }

    getStuff(id, expand) {
        return this.restApi.repository('stuff').get(id, {expand});
    }

    searchStuff(query, expand) {
        return this.restApi.repository('stuff').search(query, {expand});
    }

    getContainersList(expand) {
        return this.restApi.repository('container').getAll({expand, "per-page": 1000, sort: "-id"});
    }

    createStuff(data) {
        return this.restApi.repository('stuff').create(data);
    }

    updateStuff(id, data) {
        return this.restApi.repository('stuff').update(id, data);
    }

    deleteStuff(id) {
        return this.restApi.repository('stuff').delete(id);
    }

    pickupStuff(id) {
        return this.restApi.repository('stuff').pickup(id);
    }

    putStuff(id) {
        return this.restApi.repository('stuff').put(id);
    }

    createImage(data) {
        return this.restApi.repository('image').create(data);
    }

    createContainer(data) {
        return this.restApi.repository('container').create(data);
    }

    updateContainer(id, data) {
        return this.restApi.repository('container').update(id, data);
    }

    deleteContainer(id) {
        return this.restApi.repository('container').delete(id);
    }

    getContainer(id, expand) {
        return this.restApi.repository('container').get(id, {expand});
    }

    createSection(data) {
        return this.restApi.repository('section').create(data);
    }

    updateSection(id, data) {
        return this.restApi.repository('section').update(id, data);
    }

    getSection(id, expand) {
        return this.restApi.repository('section').get(id, {expand});
    }

    getSectionsList(expand) {
        return this.restApi.repository('section').getAll({expand, "per-page": 1000, sort: "-id"});
    }

    deleteSection(id) {
        return this.restApi.repository('section').delete(id);
    }
}